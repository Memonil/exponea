import multiprocessing

from prometheus_client import multiprocess

backlog = 2000

workers = multiprocessing.cpu_count()
worker_class = 'uvicorn.workers.UvicornWorker'

max_requests = 1000
max_requests_jitter = 100

bind = '0.0.0.0:80'


def child_exit(server, worker):
    multiprocess.mark_process_dead(worker.pid)
