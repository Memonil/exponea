import asyncio
from typing import Any, Coroutine, Dict, List, Set, Union

from aiohttp import ClientSession
from blacksheep.messages import Response
from blacksheep.server.controllers import ApiController

from memonil.exponea.constants import ENDPOINT_HITS, EXPONEA_ENDPOINT_URL
from memonil.exponea.http_client import fetch_multiple
from memonil.exponea.utils.convert import ms_to_seconds


class BaseApiController(ApiController):
    async def hit_exponea(
        self,
        http_client: ClientSession,
        timeout: int
    ) -> Union[Response, Dict[str, int], List[Dict[str, int]]]:
        requests = fetch_multiple(
            session=http_client,
            url=EXPONEA_ENDPOINT_URL,
            amount=ENDPOINT_HITS
        )
        timeout = ms_to_seconds(timeout)
        return await self.scheduler(
            requests,
            timeout
        )

    @staticmethod
    def _cancel_not_done(
        futures: Union[List[asyncio.Future], Set[asyncio.Future]]
    ):
        """Cancel no longer needed running requests."""
        for fut in futures:
            if not fut.done():
                fut.cancel()

    async def scheduler(
        self,
        tasks: List[Coroutine],
        timeout: float
    ) -> Any:
        raise NotImplementedError
