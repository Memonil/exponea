import asyncio
from typing import Coroutine, Dict, List, Union

from aiohttp import ClientSession
from blacksheep.messages import Response
from blacksheep.server.controllers import get

from memonil.exponea.controllers.base import BaseApiController
from memonil.exponea.controllers.docs import get_docs_first
from memonil.exponea.controllers.responses.base import (
    timeout_response, unsuccessful_response
)
from memonil.exponea.docs import docs


class First(BaseApiController):
    @docs(get_docs_first)
    @get()
    async def first(
        self,
        http_client: ClientSession,
        timeout: int
    ) -> Union[Response, Dict[str, int]]:
        return await self.hit_exponea(
            http_client,
            timeout=timeout
        )

    async def scheduler(
        self,
        coros: List[Coroutine],
        timeout: float
    ) -> Union[Response, Dict[str, int]]:
        futures = [asyncio.ensure_future(f) for f in coros]
        res = None
        try:
            for fut in asyncio.as_completed(
                futures,
                timeout=timeout
            ):
                if res := await fut:
                    break
        except asyncio.TimeoutError:
            res = timeout_response()

        self._cancel_not_done(futures)
        if res:
            return res
        return unsuccessful_response()
