from blacksheep.server.openapi.common import (
    ContentInfo, EndpointDocs, ResponseInfo
)

from memonil.exponea.controllers.responses.base import TimeResponse

get_docs_within_timeout = EndpointDocs(
    summary=(
        'Send requests and wait for timeout.'
    ),
    description=(
        'Collects all successful responses '
        'that return within a given timeout. '
        'If a timeout is reached before any '
        'of the 3 requests finish, the server '
        'should return an empty array instead '
        'of an error. (This means that this '
        'endpoint should never return an error)'
    ),
    responses={
        200: ResponseInfo(
            description='Successful response',
            content=[
                ContentInfo(
                    type=TimeResponse,
                    examples=[
                        [],
                        [
                            TimeResponse(
                                time=1337
                            )
                        ], [
                            TimeResponse(
                                time=1337
                            ),
                            TimeResponse(
                                time=1338
                            )
                        ], [
                            TimeResponse(
                                time=1336
                            ),
                            TimeResponse(
                                time=1337
                            ),
                            TimeResponse(
                                time=1338
                            )
                        ]
                    ],
                )
            ],
        )
    }
)
