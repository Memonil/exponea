from blacksheep.server.openapi.common import (
    ContentInfo, EndpointDocs, ResponseInfo
)

from memonil.exponea.controllers.responses.base import (
    ErrorResponse, TimeoutResponse, TimeResponse, UnsuccessfulResponse
)

get_docs_smart = EndpointDocs(
    summary=(
        'Send request, send more if '
        'first was not successful.'
    ),
    description=(
        'Instead of firing all 3 requests '
        'at once, this endpoint will first '
        'fire only a single request to '
        'Exponea testing HTTP server. Then '
        '2 scenarios can happen: a. Received '
        'a successful response within 300 '
        'milliseconds: return the response. '
        'b. Didn\'t receive a response within '
        '300 milliseconds, or the response '
        'was not successful: fire another 2 '
        'requests to Exponea testing HTTP '
        'server. Return the first successful '
        'response from any of those 3 '
        'requests (including the first one).'
    ),
    responses={
        200: ResponseInfo(
            description='Successful response',
            content=[
                ContentInfo(
                    type=TimeResponse,
                    examples=[
                        TimeResponse(
                            time=1337
                        )
                    ],
                )
            ],
        ),
        504: ResponseInfo(
            description='Error response',
            content=[
                ContentInfo(
                    type=ErrorResponse,
                    examples=[
                        TimeoutResponse(),
                        UnsuccessfulResponse()
                    ]
                )
            ]
        )
    }
)
