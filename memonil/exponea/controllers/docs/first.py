from blacksheep.server.openapi.common import (
    ContentInfo, EndpointDocs, ResponseInfo
)

from memonil.exponea.controllers.responses.base import (
    ErrorResponse, TimeoutResponse, TimeResponse, UnsuccessfulResponse
)

get_docs_first = EndpointDocs(
    summary=(
        'Send requests and wait for '
        'first within the timeout.'
    ),
    description=(
        'Returns the first successful response '
        'that returns from Exponea testing HTTP '
        'server. If timeout is reached before '
        'any successful response was received, '
        'the endpoint should return an error.'
    ),
    responses={
        200: ResponseInfo(
            description='Successful response',
            content=[
                ContentInfo(
                    type=TimeResponse,
                    examples=[
                        TimeResponse(
                            time=1337
                        )
                    ],
                )
            ],
        ),
        504: ResponseInfo(
            description='Error response',
            content=[
                ContentInfo(
                    type=ErrorResponse,
                    examples=[
                        TimeoutResponse(),
                        UnsuccessfulResponse()
                    ]
                )
            ]
        )
    }
)
