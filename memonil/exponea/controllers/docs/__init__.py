from .all import get_docs_all  # noqa
from .first import get_docs_first  # noqa
from .smart import get_docs_smart  # noqa
from .within_timeout import get_docs_within_timeout  # noqa
