from typing import List

from blacksheep.server.openapi.common import (
    ContentInfo, EndpointDocs, ResponseInfo
)

from memonil.exponea.controllers.responses.base import (
    ErrorResponse, TimeoutResponse, TimeResponse, UnsuccessfulResponse
)

get_docs_all = EndpointDocs(
    summary=(
        'Send requests and wait for all'
        ' of them within the timeout.'
    ),
    description=(
        'Collects all successful responses from '
        'Exponea testing HTTP server and returns '
        'the result as an array. If timeout is '
        'reached before all requests finish, or '
        'none of the responses were successful, '
        'the endpoint should return an error.'
    ),
    responses={
        200: ResponseInfo(
            description='Successful responses',
            content=[
                ContentInfo(
                    type=List[TimeResponse],
                    examples=[
                        [
                            TimeResponse(
                                time=1337
                            )
                        ], [
                            TimeResponse(
                                time=1336
                            ),
                            TimeResponse(
                                time=1337
                            ),
                        ], [
                            TimeResponse(
                                time=1336
                            ),
                            TimeResponse(
                                time=1337
                            ),
                            TimeResponse(
                                time=1338
                            )
                        ]
                    ],
                )
            ],
        ),
        504: ResponseInfo(
            description='Error response',
            content=[
                ContentInfo(
                    type=ErrorResponse,
                    examples=[
                        TimeoutResponse(),
                        UnsuccessfulResponse()
                    ]
                )
            ]
        )
    }
)
