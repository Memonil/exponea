import asyncio
from typing import Coroutine, Dict, List, Union

from aiohttp import ClientSession
from blacksheep.messages import Response
from blacksheep.server.controllers import get

from memonil.exponea.controllers.base import BaseApiController
from memonil.exponea.controllers.docs import get_docs_all
from memonil.exponea.controllers.responses.base import (
    timeout_response, unsuccessful_response
)
from memonil.exponea.docs import docs


class All(BaseApiController):
    @docs(get_docs_all)
    @get()
    async def all(
        self,
        http_client: ClientSession,
        timeout: int
    ) -> Union[Response, List[Dict[str, int]]]:
        return await self.hit_exponea(
            http_client,
            timeout=timeout
        )

    async def scheduler(
        self,
        coros: List[Coroutine],
        timeout: float
    ) -> Union[Response, List[Dict[str, int]]]:
        futures = [asyncio.ensure_future(f) for f in coros]
        done, pending = await asyncio.wait(
            futures,
            timeout=timeout,
            return_when=asyncio.ALL_COMPLETED
        )
        results = []
        if pending:
            self._cancel_not_done(pending)
            return timeout_response()

        for task in done:
            if res := task.result():
                results.append(res)

        if not results:
            return unsuccessful_response()
        return results
