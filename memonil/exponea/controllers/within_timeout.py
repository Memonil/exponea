import asyncio
from typing import Coroutine, Dict, List, Optional, Union

from aiohttp import ClientSession
from blacksheep.server.controllers import get
from blacksheep.utils import join_fragments

from memonil.exponea.controllers.base import BaseApiController
from memonil.exponea.controllers.docs import get_docs_within_timeout
from memonil.exponea.docs import docs


class WithinTimeout(BaseApiController):
    @docs(get_docs_within_timeout)
    @get()
    async def within_timeout(
        self,
        http_client: ClientSession,
        timeout: int
    ) -> Union[List[Dict[str, int]]]:
        return await self.hit_exponea(
            http_client,
            timeout=timeout
        )

    async def scheduler(
        self,
        coros: List[Coroutine],
        timeout: float
    ) -> List[Dict[str, int]]:
        futures = [asyncio.ensure_future(f) for f in coros]
        done, pending = await asyncio.wait(
            futures,
            timeout=timeout,
            return_when=asyncio.ALL_COMPLETED
        )
        results = []
        for task in done:
            if res := task.result():
                results.append(res)

        if pending:
            self._cancel_not_done(pending)
        return results

    @classmethod
    def route(cls) -> Optional[str]:
        cls_name = cls.__name__
        cls_name = ''.join(['-' + c.lower() if c.isupper() else c
                            for c in cls_name]).lstrip('-')
        return join_fragments('api', cls_name)
