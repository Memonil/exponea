from blacksheep.messages import Response
from blacksheep.server.responses import json
from pydantic import BaseModel


class TimeResponse(BaseModel):
    time: int


class ErrorResponse(BaseModel):
    error: str


class TimeoutResponse(ErrorResponse):
    error: str = 'Requests have exceeded timeout period'


class UnsuccessfulResponse(ErrorResponse):
    error: str = 'Requests responses were unsuccessful'


# blacksheep's Response class must
# be instantiated for every request
def timeout_response() -> Response:
    return json(TimeoutResponse(), status=504)


def unsuccessful_response() -> Response:
    return json(UnsuccessfulResponse(), status=504)
