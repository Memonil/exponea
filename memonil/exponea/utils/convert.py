def ms_to_seconds(timeout: int) -> float:
    return timeout / 1000.0
