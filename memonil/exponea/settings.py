from envparse import env

sentry_dsn = env('SENTRY_DSN', '')

datadog = env.bool('ENABLE_DATADOG', default=False)
