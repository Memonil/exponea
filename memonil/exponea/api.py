from blacksheep.server import Application
from ddtrace_asgi.middleware import TraceMiddleware
import sentry_sdk
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

# do controllers import to make blacksheep able to recognize them
from memonil.exponea import controllers  # noqa # pylint: disable=unused-import
from memonil.exponea.docs import docs
from memonil.exponea.http_client import (
    configure_http_client, dispose_http_client
)
from memonil.exponea.middleware import (
    data_to_response, logger, prometheus_statistics
)
from memonil.exponea.prometheus import Prometheus
from memonil.exponea.settings import datadog, sentry_dsn

app = Application(show_error_details=True)

docs.bind_app(app)

app.on_start += configure_http_client
app.on_stop += dispose_http_client

app.services.add_instance(Prometheus(app))

app.middlewares.append(logger)
app.middlewares.append(prometheus_statistics)
app.middlewares.append(data_to_response)

if datadog:
    app = TraceMiddleware(app, service='exponea')

sentry_sdk.init(sentry_dsn)
SentryAsgiMiddleware(app)
