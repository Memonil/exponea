from typing import Coroutine, Dict, List, Union

from aiohttp import ClientSession
from blacksheep.server import Application
import structlog

log = structlog.get_logger()


async def configure_http_client(app: Application) -> None:
    """Initiate one session instance per worker."""
    http_client = ClientSession()
    app.services.add_instance(http_client)


async def dispose_http_client(app: Application) -> None:
    """Close session on shutdown."""
    http_client: ClientSession = app.service_provider.get(ClientSession)
    await http_client.close()


async def fetch(
    session: ClientSession,
    url: str
) -> Union[None, Dict]:
    async with session.get(
        url=url
    ) as response:
        log.info(
            'client_request',
            method=response.method,
            status=response.status,
            url=str(response.url)
        )
        if response.status == 200:
            resp = await response.json()
            return resp


def fetch_multiple(
    session: ClientSession,
    url: str,
    amount: int
) -> List[Coroutine]:
    coros = [fetch(session, url)
             for _ in range(0, amount)]
    return coros
