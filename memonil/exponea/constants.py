DOCS_TITLE = 'Exponea API requester'
DOCS_VERSION = '0.0.1'
DOCS_DESCRIPTION = 'Exponea API responses tests'

EXPONEA_ENDPOINT_URL = (
    'https://exponea-engineering-assignment.appspot.com/api/work'
)
ENDPOINT_HITS = 3
SMART_ENDPOINT_TIMEOUT = .3
