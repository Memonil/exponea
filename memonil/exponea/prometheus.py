from contextlib import contextmanager
import os
from typing import ContextManager

from blacksheep.contents import Content
from blacksheep.messages import Response
from blacksheep.server import Application
from prometheus_client import (
    CollectorRegistry, Counter, Gauge, generate_latest, Histogram, REGISTRY
)
from prometheus_client.multiprocess import MultiProcessCollector


class Prometheus:
    def __init__(self, app: Application) -> None:
        self.registry = None
        self.configure_registry()

        self._total = Counter(
            name='http_requests_total',
            documentation=(
                'Total number of requests by method, '
                'status and endpoint.'
            ),
            labelnames=(
                'method',
                'status',
                'endpoint'
            ),
            registry=self.registry
        )
        self.latency_avg = Histogram(
            name='http_request_duration_avg',
            documentation=(
                'Latency with of all endpoints.'
            ),
            registry=self.registry
        )
        self.latency_agg = Histogram(
            name='http_request_duration_by_endpoint',
            documentation=(
                'Latency aggregated by endpoint.'
            ),
            labelnames=('endpoint',),
            registry=self.registry
        )

        self._inprogress = Gauge(
            name='http_requests_inprogress',
            documentation=(
                'Number of HTTP requests in progress.'
            ),
            labelnames=(
                'method',
                'endpoint'
            ),
            multiprocess_mode='livesum',
            registry=self.registry
        )

        @app.router.get('/metrics')
        def metrics() -> Response:  # pylint: disable=unused-variable
            content = generate_latest(self.registry)
            return Response(
                200,
                content=Content(
                    b'text/plain',
                    content
                )
            )

    def configure_registry(self):
        if os.getenv('prometheus_multiproc_dir'):
            self.registry = CollectorRegistry()
            MultiProcessCollector(self.registry)
        else:
            self.registry = REGISTRY

    def total(
        self,
        method: str,
        status: str,
        endpoint: str,
    ) -> None:
        self._total.labels(
            method,
            status,
            endpoint
        ).inc()

    @contextmanager
    def inprogress(
        self,
        method: str,
        endpoint: str
    ) -> ContextManager[None]:
        _inprogress = self._inprogress.labels(
            method,
            endpoint
        )
        _inprogress.inc()
        yield
        _inprogress.dec()

    def latency(
        self,
        duration: float,
        endpoint: str
    ) -> None:
        self.latency_avg.observe(duration)
        self.latency_agg.labels(
            endpoint
        ).observe(duration)

    @contextmanager
    def instrumentations(
        self,
        method: str,
        endpoint: str,
        status: str,
        duration: float
    ) -> None:
        self.latency(
            duration,
            endpoint
        )
        self.total(
            method,
            status,
            endpoint
        )
