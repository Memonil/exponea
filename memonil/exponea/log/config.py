import json
import logging
import logging.config

import structlog

from .processors import (
    drop_debug_logs, float_rounder, process_stdlib_logging, unix_timestamper
)

PRODUCTION_PROCESSORS = [
    structlog.stdlib.add_log_level,
    drop_debug_logs,
    structlog.stdlib.PositionalArgumentsFormatter(),
    unix_timestamper,
    float_rounder,
    structlog.processors.format_exc_info,
    structlog.processors.UnicodeDecoder(),
    structlog.processors.JSONRenderer(serializer=json.dumps),
]


def configure_structlog() -> None:
    """Configure proper log processors and settings for structlog."""

    structlog.configure_once(
        processors=PRODUCTION_PROCESSORS,
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
        context_class=dict,
    )


def configure_stdlib_logging() -> None:
    """Configure standard logging to use the same processors as structlog."""

    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'plain': {
                '()': structlog.stdlib.ProcessorFormatter,
                'processor': process_stdlib_logging,
            },
        },
        'handlers': {
            'default': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'plain',
                'stream': 'ext://sys.stdout'
            },
        },
        'loggers': {
            '': {
                'handlers': ['default'],
                'level': 'INFO',
                'propagate': True,
            },
        },
    })
