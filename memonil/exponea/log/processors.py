from time import time

import structlog


def float_rounder(_, __, event_dict: dict) -> dict:
    """Round any floats in ``event_dict`` to 3 decimal places."""
    for key, value in list(event_dict.items()):
        if isinstance(value, float):
            event_dict[key] = round(value, 3)
    return event_dict


def drop_debug_logs(_, __, event_dict: dict) -> dict:
    """Drop event with ``debug`` log level."""
    if event_dict['level'] == 'debug':
        raise structlog.DropEvent
    return event_dict


def unix_timestamper(_, __, event_dict: dict) -> dict:
    """Add current timestamp to event."""
    event_dict['timestamp'] = time()
    return event_dict


def process_stdlib_logging(_, __, event_dict: dict) -> dict:
    return event_dict
