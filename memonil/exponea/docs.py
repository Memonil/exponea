from blacksheep.server.openapi.v3 import OpenAPIHandler
from openapidocs.v3 import Info

from memonil.exponea.constants import (
    DOCS_DESCRIPTION, DOCS_TITLE, DOCS_VERSION
)

docs = OpenAPIHandler(
    info=Info(
        title=DOCS_TITLE,
        version=DOCS_VERSION,
        description=DOCS_DESCRIPTION
    )
)
