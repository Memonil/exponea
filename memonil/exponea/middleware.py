from timeit import default_timer
from typing import Any

from blacksheep.messages import Request, Response
from blacksheep.server.responses import json
import structlog

from memonil.exponea.prometheus import Prometheus

log = structlog.get_logger()


async def logger(
    request: Request,
    handler: Any
) -> Response:
    log.info(
        'request',
        method=request.method,
        path=request._path
    )
    response = await handler(request)
    log.info(
        'response',
        status=response.status
    )
    return response


async def data_to_response(
    request: Request,
    handler: Any
) -> Response:
    """Let handlers return custom data.

    Transform them to blacksheep.messages.Response class.
    """
    response = await handler(request)
    if not isinstance(response, Response):
        response = json(response)
    return response


async def prometheus_statistics(
    request: Request,
    handler: Any,
    prometheus: Prometheus
) -> Response:
    """Collect statistics to Prometheus."""
    method = request.method
    endpoint = request._path.decode('utf-8')
    with prometheus.inprogress(
        method,
        endpoint
    ):
        start_time = default_timer()
        response = await handler(request)
        duration = default_timer() - start_time
    prometheus.instrumentations(
        method,
        endpoint,
        response.status,
        duration
    )
    return response
