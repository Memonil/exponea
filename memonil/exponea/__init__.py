from .log import configure_stdlib_logging, configure_structlog

configure_structlog()
configure_stdlib_logging()
