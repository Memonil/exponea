import pytest
import structlog

from memonil.exponea.log import processors as uut


def test_float_rounder():
    params = {'time': 123.456789}
    result = uut.float_rounder(None, None, params)
    assert result == {'time': round(123.456789, 3)}


def test_drop_debug_logs():
    params = {'level': 'info'}
    result = uut.drop_debug_logs(None, 'info', params)
    assert result == params

    params = {'level': 'debug'}
    with pytest.raises(structlog.DropEvent):
        uut.drop_debug_logs(None, 'debug', params)


def test_unix_timestamper(mocker):
    mocker.patch.object(uut, 'time', return_value=1234567890.0)
    result = uut.unix_timestamper(None, None, {})
    assert result == {'timestamp': 1234567890.0}


def test_process_stdlib_logging():
    params = {'event': 'test'}
    result = uut.process_stdlib_logging(None, None, params)
    assert result == {'event': 'test'}
