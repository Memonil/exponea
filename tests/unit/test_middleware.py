from contextlib import contextmanager
from unittest.mock import AsyncMock, Mock

from blacksheep.messages import Request, Response
import pytest

from memonil.exponea import middleware as uut
from memonil.exponea.prometheus import Prometheus

pytestmark = pytest.mark.asyncio


async def test_logger(log):
    request = Mock(spec=Request)
    request.method = 'GET'
    request._path = '/'
    response = AsyncMock(spec=Response)
    response.return_value.status = 200

    await uut.logger(request, response)
    assert log.has(
        'request',
        method='GET',
        path=request._path
    )
    assert log.has(
        'response',
        status=200
    )


async def test_data_to_response():
    request = Mock(spec=Request)
    response = AsyncMock()
    response.return_value = AsyncMock(spec=Response)
    resp = await uut.data_to_response(request, response)
    assert isinstance(resp, Response)

    # transform list
    response = AsyncMock()
    response.return_value = []
    resp = await uut.data_to_response(request, response)
    assert isinstance(resp, Response)

    # transform dict
    response = AsyncMock()
    response.return_value = {}
    resp = await uut.data_to_response(request, response)
    assert isinstance(resp, Response)


async def test_prometheus_statistics(mocker):

    @contextmanager
    def inprogress(method, endpoint):
        yield

    request = Mock(spec=Request)
    request.method = 'GET'
    request._path = b'/'
    response = AsyncMock(spec=Response)
    response.status = 200
    prometheus = Mock(spec=Prometheus)
    mocker.patch.object(uut, 'default_timer', return_value=1)
    prometheus.inprogress.side_effect = inprogress

    response = await uut.prometheus_statistics(request, response, prometheus)
    prometheus.inprogress.assert_called_once_with(
        request.method,
        request._path.decode('utf-8')
    )
    prometheus.instrumentations.assert_called_once_with(
        request.method,
        request._path.decode('utf-8'),
        response.status,
        0
    )
