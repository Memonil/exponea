import asyncio
from unittest.mock import Mock

from aiohttp import ClientSession
import pytest

from memonil.exponea import http_client as uut

pytestmark = pytest.mark.asyncio


async def test_fetch(monkeypatch, log):

    class RequestMock:
        def __init__(self, status, data):
            self.status = status
            self.data = data
            self.url = 'localhost'
            self.method = 'GET'

        async def __aenter__(self):
            return self

        async def __aexit__(self, exc_type, exc_val, exc_tb):
            return self

        async def json(self):
            return self.data

    def get(url):
        return RequestMock(int(url), {'time': 1337})

    session = Mock(spec=ClientSession)
    monkeypatch.setattr(session, 'get', get)

    resp = await uut.fetch(session, '200')
    assert resp == {'time': 1337}
    assert log.has(
        'client_request',
        method='GET',
        status=200,
        url='localhost'
    )

    resp = await uut.fetch(session, '500')
    assert not resp
    assert log.has(
        'client_request',
        method='GET',
        status=500,
        url='localhost'
    )


async def test_fetch_multiple(monkeypatch):

    async def fetch(session, url):
        pass

    monkeypatch.setattr(uut, 'fetch', fetch)
    amount = 3
    coros = uut.fetch_multiple(Mock(), Mock(), amount)
    assert all(asyncio.iscoroutine(coro)
               for coro in coros)
    assert len(coros) == amount

    # cleanup coros
    for coro in coros:
        coro.close()
