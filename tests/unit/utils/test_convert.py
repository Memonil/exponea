from memonil.exponea.utils import convert as uut


def test_ms_to_seconds():
    milliseconds = 1337
    seconds = uut.ms_to_seconds(milliseconds)
    assert isinstance(seconds, float)
    assert seconds == milliseconds / 1000.0
