import os
from unittest.mock import Mock

from blacksheep.server import Application
from prometheus_client import CollectorRegistry, Counter, Gauge, Histogram

from memonil.exponea import prometheus as uut


class TestPrometheus:
    app = Application()
    os.environ['prometheus_multiproc_dir'] = '/tmp'
    cls = uut.Prometheus(app)

    def test_init(self):
        assert isinstance(self.cls.registry, CollectorRegistry)
        assert isinstance(self.cls._total, Counter)
        assert isinstance(self.cls.latency_avg, Histogram)
        assert isinstance(self.cls.latency_agg, Histogram)
        assert isinstance(self.cls._inprogress, Gauge)
        routes = self.app.router.routes.get(b'GET')
        assert any([route.pattern == b'/metrics'
                    for route in routes])

    def test_configure_registry(self, mocker):
        self.cls.configure_registry()
        assert isinstance(self.cls.registry, CollectorRegistry)

        del os.environ['prometheus_multiproc_dir']
        mocker.patch.object(uut, 'REGISTRY', Mock())
        self.cls.configure_registry()
        assert isinstance(self.cls.registry, Mock)

    def test_total(self, mocker):
        method = Mock()
        status = Mock()
        endpoint = Mock()
        total = Mock(wraps=self.cls._total)
        counter = Mock(spec=Counter)
        total.labels.return_value = counter
        mocker.patch.object(self.cls, '_total', total)

        self.cls.total(
            method,
            status,
            endpoint
        )
        total.labels.assert_called_once_with(
            method,
            status,
            endpoint
        )
        counter.inc.assert_called_once()

    def test_inprogress(self, mocker):
        method = Mock()
        status = Mock()
        inprogress = Mock(wraps=self.cls._inprogress)
        gauge = Mock(spec=Gauge)
        inprogress.labels.return_value = gauge
        mocker.patch.object(self.cls, '_inprogress', inprogress)
        with self.cls.inprogress(method, status):
            pass
        inprogress.labels.assert_called_once_with(
            method,
            status
        )
        gauge.inc.assert_called_once()
        gauge.dec.assert_called_once()

    def test_latency(self, mocker):
        duration = .1
        endpoint = Mock()
        latency_avg = Mock(wraps=self.cls.latency_avg)
        latency_agg = Mock(wraps=self.cls.latency_agg)
        histogram = Mock(spec=Histogram)
        latency_agg.labels.return_value = histogram
        mocker.patch.object(self.cls, 'latency_avg', latency_avg)
        mocker.patch.object(self.cls, 'latency_agg', latency_agg)

        self.cls.latency(
            duration,
            endpoint
        )
        latency_avg.observe.assert_called_once_with(
            duration
        )
        latency_agg.labels.assert_called_once_with(
            endpoint
        )
        histogram.observe.assert_called_once_with(
            duration
        )

    def test_instrumentations(self, mocker):
        method = Mock()
        endpoint = Mock()
        status = Mock()
        duration = .1
        total = Mock(wraps=self.cls.total)
        latency = Mock(wraps=self.cls.latency)

        mocker.patch.object(self.cls, 'latency', latency)
        mocker.patch.object(self.cls, 'total', total)

        self.cls.instrumentations(
            method,
            endpoint,
            status,
            duration
        )
        latency.assert_called_once_with(
            duration,
            endpoint
        )
        total.assert_called_once_with(
            method,
            status,
            endpoint
        )
