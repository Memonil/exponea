import asyncio

from blacksheep.server.responses import Response
import pytest

from memonil.exponea.controllers import first as uut


class TestFirst:
    cls = uut.First()

    @pytest.mark.asyncio
    async def test_scheduler(self):

        async def request(body, empty=False, delay=.1):
            nonlocal cancelled
            try:
                await asyncio.sleep(delay)
                if empty:
                    return
                return body
            except asyncio.CancelledError:
                # task got cancelled
                cancelled += 1
                raise

        beginning_tasks = asyncio.all_tasks()
        cancelled = 0

        # first task is successful
        coros = [
            request('first'),
            request('second'),
            request('third')
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == 'first'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        # second task is successful
        coros = [
            request('first', empty=True),
            request('second'),
            request('third', empty=True)
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == 'second'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        # third task is successful
        coros = [
            request('first', empty=True),
            request('second', empty=True),
            request('third')
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == 'third'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        # first task is successful, others cancelled
        coros = [
            request('first', delay=.05),
            request('second'),
            request('third', empty=True),
        ]
        resp = await self.cls.scheduler(
            coros,
            .06
        )
        assert resp == 'first'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 2

        cancelled = 0
        # first task is successful, last cancelled
        coros = [
            request('first', delay=.05),
            request('second', delay=.05),
            request('third', empty=True),
        ]
        resp = await self.cls.scheduler(
            coros,
            .06
        )
        assert resp == 'first'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 1

        cancelled = 0
        # second task is successful, others cancelled
        coros = [
            request('first'),
            request('second', delay=.05),
            request('third', empty=True),
        ]
        resp = await self.cls.scheduler(
            coros,
            .06
        )
        assert resp == 'second'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 2

        cancelled = 0
        # second task is successful, last cancelled
        coros = [
            request('first', delay=.05, empty=True),
            request('second', delay=.05),
            request('third'),
        ]
        resp = await self.cls.scheduler(
            coros,
            .06
        )
        assert resp == 'second'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 1

        cancelled = 0
        # third task is successful, others cancelled
        coros = [
            request('first'),
            request('second'),
            request('third', delay=.05),
        ]
        resp = await self.cls.scheduler(
            coros,
            .06
        )
        assert resp == 'third'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 2

        cancelled = 0
        # third task is successful, first cancelled
        coros = [
            request('first'),
            request('second', delay=.05, empty=True),
            request('third', delay=.05),
        ]
        resp = await self.cls.scheduler(
            coros,
            .06
        )
        assert resp == 'third'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 1

        cancelled = 0
        # all tasks are unsuccessful
        coros = [
            request('first', empty=True),
            request('second', empty=True),
            request('third', empty=True),
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert isinstance(resp, Response)
        assert resp.status == 504
        assert await resp.json() == {
            'error': 'Requests responses were unsuccessful'
        }
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        cancelled = 0
        # all tasks timed out
        coros = [
            request('first'),
            request('second'),
            request('third'),
        ]
        resp = await self.cls.scheduler(
            coros,
            .09
        )
        assert isinstance(resp, Response)
        assert resp.status == 504
        assert await resp.json() == {
            'error': 'Requests have exceeded timeout period'
        }
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 3

        # check if timed out tasks are properly cancelled
        assert asyncio.all_tasks() == beginning_tasks
