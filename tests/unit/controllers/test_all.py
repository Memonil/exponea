import asyncio

from blacksheep.server.responses import Response
import pytest

from memonil.exponea.controllers import all as uut


class TestAll:
    cls = uut.All()

    @pytest.mark.asyncio
    async def test_scheduler(self):

        async def request(body, empty=False):
            nonlocal cancelled
            try:
                await asyncio.sleep(.1)
                if empty:
                    return
                return body
            except asyncio.CancelledError:
                # task got cancelled
                cancelled += 1
                raise

        beginning_tasks = asyncio.all_tasks()
        cancelled = 0

        # all tasks should finish before timeout
        coros = [
            request('first'),
            request('second'),
            request('third'),
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert sorted(resp) == sorted(['first', 'second', 'third'])
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        # two tasks should finish before timeout
        coros = [
            request('first', empty=True),
            request('second'),
            request('third'),
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert sorted(resp) == sorted(['second', 'third'])
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        # one task should finish before timeout
        coros = [
            request('first', empty=True),
            request('second'),
            request('third', empty=True),
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == ['second']
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        cancelled = 0
        # tasks will not finish before timeout
        coros = [
            request('first'),
            request('second'),
            request('third'),
        ]
        resp = await self.cls.scheduler(
            coros,
            .05
        )
        assert isinstance(resp, Response)
        assert resp.status == 504
        assert await resp.json() == {
            'error': 'Requests have exceeded timeout period'
        }
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 3

        cancelled = 0
        # tasks were unsuccessful
        coros = [
            request('first', empty=True),
            request('second', empty=True),
            request('third', empty=True),
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert isinstance(resp, Response)
        assert resp.status == 504
        assert await resp.json() == {
            'error': 'Requests responses were unsuccessful'
        }
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        # check if timed out tasks are properly cancelled
        assert asyncio.all_tasks() == beginning_tasks
