import asyncio

import pytest

from memonil.exponea.controllers import within_timeout as uut


class TestWithinTimeout:
    cls = uut.WithinTimeout()

    @pytest.mark.asyncio
    async def test_scheduler(self):

        async def request(body, empty=False, delay=.1):
            nonlocal cancelled
            try:
                await asyncio.sleep(delay)
                if empty:
                    return
                return body
            except asyncio.CancelledError:
                # task got cancelled
                cancelled += 1
                raise

        beginning_tasks = asyncio.all_tasks()
        cancelled = 0

        # all tasks should finish before timeout
        coros = [
            request('first'),
            request('second'),
            request('third')
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert sorted(resp) == sorted(['first', 'second', 'third'])
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        # one task timed out
        coros = [
            request('first'),
            request('second', delay=.12),
            request('third')
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert sorted(resp) == sorted(['first', 'third'])
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 1

        cancelled = 0
        # one task was unsuccessful
        coros = [
            request('first', empty=True),
            request('second'),
            request('third')
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert sorted(resp) == sorted(['second', 'third'])
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        cancelled = 0
        # two tasks timed out
        coros = [
            request('first', delay=.12),
            request('second'),
            request('third', delay=.12)
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == ['second']
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 2

        cancelled = 0
        # all tasks timed out
        coros = [
            request('first', delay=.12),
            request('second', delay=.12),
            request('third', delay=.12)
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == []
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 3

        cancelled = 0
        # one task timed out, other was unsuccessful
        coros = [
            request('first', delay=.12),
            request('second'),
            request('third', empty=True)
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == ['second']
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 1

        cancelled = 0
        # one task timed out, others were unsuccessful
        coros = [
            request('first', empty=True),
            request('second', delay=.12),
            request('third', empty=True)
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == []
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 1

        cancelled = 0
        # two tasks timed out, other was unsuccessful
        coros = [
            request('first', empty=True),
            request('second', delay=.12),
            request('third', delay=.12)
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == []
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 2

        cancelled = 0
        # two tasks were unsuccessful
        coros = [
            request('first', empty=True),
            request('second', empty=True),
            request('third')
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == ['third']
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        # all tasks were unsuccessful
        coros = [
            request('first', empty=True),
            request('second', empty=True),
            request('third', empty=True)
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == []
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0

        # check if timed out tasks are properly cancelled
        assert asyncio.all_tasks() == beginning_tasks
