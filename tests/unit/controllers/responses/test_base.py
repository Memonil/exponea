from blacksheep.messages import Response
import pytest

from memonil.exponea.controllers.responses import base as uut

pytestmark = pytest.mark.asyncio


async def test_timeout_response():
    response = uut.timeout_response()
    assert isinstance(response, Response)
    assert response.status == 504
    assert await response.json() == {
        'error': 'Requests have exceeded timeout period'
    }


async def test_unsuccessful_response():
    response = uut.unsuccessful_response()
    assert isinstance(response, Response)
    assert response.status == 504
    assert await response.json() == {
        'error': 'Requests responses were unsuccessful'
    }
