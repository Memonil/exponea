import asyncio

from blacksheep.server.responses import Response
import pytest

from memonil.exponea.controllers import smart as uut


class TestSmart:
    cls = uut.Smart()

    @pytest.mark.asyncio
    async def test_scheduler(self):
        async def request(body, empty=False, delay=.3):
            nonlocal cancelled, called
            called += 1
            try:
                await asyncio.sleep(delay)
                if empty:
                    return
                return body
            except asyncio.CancelledError:
                # task got cancelled
                cancelled += 1
                raise

        beginning_tasks = asyncio.all_tasks()
        cancelled, called = 0, 0

        # first task is successful, others not even called
        coros = [
            request('first', delay=.02),
            request('second'),
            request('third')
        ]
        resp = await self.cls.scheduler(
            coros,
            .11
        )
        assert resp == 'first'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0
        assert called == 1

        called = 0
        # second task is successful, others cancelled
        coros = [
            request('first', delay=.31),
            request('second', delay=.05),
            request('third')
        ]
        resp = await self.cls.scheduler(
            coros,
            .1
        )
        assert resp == 'second'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 2
        assert called == 3

        cancelled, called = 0, 0
        # second task is successful, third cancelled
        coros = [
            request('first', delay=.05, empty=True),
            request('second', delay=.05),
            request('third')
        ]
        resp = await self.cls.scheduler(
            coros,
            .1
        )
        assert resp == 'second'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 1
        assert called == 3

        cancelled, called = 0, 0
        # third task is successful, others cancelled
        coros = [
            request('first', delay=.31),
            request('second', delay=.06),
            request('third', delay=.04)
        ]
        resp = await self.cls.scheduler(
            coros,
            .05
        )
        assert resp == 'third'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 2
        assert called == 3

        cancelled, called = 0, 0
        # third task is successful, second cancelled
        coros = [
            request('first', delay=.04, empty=True),
            request('second', delay=.06),
            request('third', delay=.04)
        ]
        resp = await self.cls.scheduler(
            coros,
            .05
        )
        assert resp == 'third'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 1
        assert called == 3

        cancelled, called = 0, 0
        # third task is successful, first cancelled
        coros = [
            request('first', delay=.31),
            request('second', delay=.04, empty=True),
            request('third', delay=.04)
        ]
        resp = await self.cls.scheduler(
            coros,
            .05
        )
        assert resp == 'third'
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 1
        assert called == 3

        cancelled, called = 0, 0
        # all tasks are unsuccessful
        coros = [
            request('first', delay=.04, empty=True),
            request('second', delay=.04, empty=True),
            request('third', delay=.04, empty=True)
        ]
        resp = await self.cls.scheduler(
            coros,
            .05
        )
        assert isinstance(resp, Response)
        assert resp.status == 504
        assert await resp.json() == {
            'error': 'Requests responses were unsuccessful'
        }
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 0
        assert called == 3

        called = 0
        # all tasks timed out
        coros = [
            request('first', delay=.31),
            request('second', delay=.06),
            request('third', delay=.06)
        ]
        resp = await self.cls.scheduler(
            coros,
            .05
        )
        assert isinstance(resp, Response)
        assert resp.status == 504
        assert await resp.json() == {
            'error': 'Requests have exceeded timeout period'
        }
        futures = [asyncio.ensure_future(f) for f in coros]
        await asyncio.wait(futures)
        assert cancelled == 3
        assert called == 3

        # check if timed out tasks are properly cancelled
        assert asyncio.all_tasks() == beginning_tasks
