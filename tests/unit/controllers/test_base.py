import asyncio
from unittest.mock import Mock

import pytest

from memonil.exponea.controllers import base as uut

pytestmark = pytest.mark.asyncio


class TestBaseApiController:
    cls = uut.BaseApiController()

    async def test_hit_exponea(self, mocker):

        async def dummy():
            pass

        def fetch_multiple(session, url, amount):
            return coros

        coros = [dummy() for _ in range(3)]
        timeout = 1

        mocker.patch.object(uut, 'fetch_multiple', fetch_multiple)
        scheduler = mocker.patch.object(self.cls, 'scheduler')
        await self.cls.hit_exponea(Mock(), timeout)
        scheduler.assert_called_with(coros, timeout / 1000.0)

        # cleanup
        for coro in coros:
            coro.close()

    async def test_cancel_not_done(self):
        async def dummy():
            while True:
                continue

        coros = [dummy() for _ in range(3)]
        futures = [asyncio.ensure_future(f) for f in coros]
        assert not any([fut.cancelled() for fut in futures])
        self.cls._cancel_not_done(futures)
        await asyncio.wait(futures)  # wait for cancellation
        assert all([fut.cancelled() for fut in futures])

    async def test_scheduler(self):
        with pytest.raises(NotImplementedError):
            await self.cls.scheduler(Mock(), Mock())
