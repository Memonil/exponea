import asyncio
from typing import Any
from unittest.mock import AsyncMock, patch

from blacksheep.contents import ASGIContent
from blacksheep.messages import Request
from blacksheep.scribe import send_asgi_response
from blacksheep.server import Application
import pytest
from starlette.testclient import TestClient as BaseTestClient

from memonil.exponea.api import app  # Ignore PyImportSortBear


async def __call__(self, scope, receive, send):
    """Replace blacksheep's __call__ method with asgi compatible one.

    Blacksheep uses term `raw_path` instead of `path`
    and expects type `bytes` type instead of type `str`.
    """
    if scope['type'] == 'lifespan':
        return await self._handle_lifespan(receive, send)

    assert scope['type'] == 'http'

    request = Request.incoming(
        scope['method'],
        scope['path'].encode('utf-8'),
        scope['query_string'],
        scope['headers']
    )
    request.scope = scope
    request.content = ASGIContent(receive)

    response = await self.handle(request)
    await send_asgi_response(response, send)

    request.scope = None  # type: ignore
    request.content.dispose()


class TestClient(BaseTestClient):
    """Replace Starlette's context managers with self.loop.

    Unit tests are most likely creating another instance
    of loop and that will make Starlette's shutdown
    to fail because it has running stuff on different loop.
    """

    def __enter__(self) -> "TestClient":
        self.loop = asyncio.get_event_loop()
        self.send_queue = asyncio.Queue()  # type: asyncio.Queue
        self.receive_queue = asyncio.Queue()  # type: asyncio.Queue
        self.task = self.loop.create_task(self.lifespan())
        self.loop.run_until_complete(self.wait_startup())
        return self

    def __exit__(self, *args: Any) -> None:
        self.loop.run_until_complete(self.wait_shutdown())


@pytest.fixture(scope='session', autouse=True)
def client() -> TestClient:
    with patch.object(Application, '__call__', __call__):
        with TestClient(app) as client:
            yield client


@pytest.fixture
def supermocker(mocker):
    async_mock = AsyncMock()

    def replace(obj, target):
        mocker.patch.object(obj, target, async_mock)
        return async_mock

    yield replace
