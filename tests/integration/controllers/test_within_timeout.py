from memonil.exponea.controllers import within_timeout as uut


class TestWithinTimeout:

    @staticmethod
    def test_within_timeout(client, supermocker):
        mocked = supermocker(
            uut.WithinTimeout,
            'hit_exponea'
        )
        mocked.return_value = [{'time': 1337}]
        resp = client.get('/api/within-timeout?timeout=500')
        assert resp.json() == [{'time': 1337}]
        assert resp.status_code == 200

        mocked.return_value = []
        resp = client.get('/api/within-timeout?timeout=500')
        assert resp.json() == []
        assert resp.status_code == 200
