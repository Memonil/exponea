from blacksheep.server.responses import json

from memonil.exponea.controllers import all as uut


class TestAll:

    @staticmethod
    def test_total(client, supermocker):
        mocked = supermocker(
            uut.All,
            'hit_exponea'
        )
        mocked.return_value = [{'time': 1337}]
        resp = client.get('/api/all?timeout=500')
        assert resp.json() == [{'time': 1337}]
        assert resp.status_code == 200

        mocked.return_value = json({
            'error': 'Requests have exceeded timeout period'
        }, status=504)
        resp = client.get('/api/all?timeout=500')
        assert resp.json() == {
            'error': 'Requests have exceeded timeout period'
        }
        assert resp.status_code == 504

        mocked.return_value = json({
            'error': 'Requests responses were unsuccessful'
        }, status=504)
        resp = client.get('/api/all?timeout=500')
        assert resp.json() == {
            'error': 'Requests responses were unsuccessful'
        }
        assert resp.status_code == 504
