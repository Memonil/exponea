from blacksheep.server.responses import json

from memonil.exponea.controllers import first as uut


class TestFirst:

    @staticmethod
    def test_first(client, supermocker):
        mocked = supermocker(
            uut.First,
            'hit_exponea'
        )
        mocked.return_value = [{'time': 1337}]
        resp = client.get('/api/first?timeout=500')
        assert resp.json() == [{'time': 1337}]
        assert resp.status_code == 200

        mocked.return_value = json({
            'error': 'Requests have exceeded timeout period'
        }, status=504)
        resp = client.get('/api/first?timeout=500')
        assert resp.json() == {
            'error': 'Requests have exceeded timeout period'
        }
        assert resp.status_code == 504
