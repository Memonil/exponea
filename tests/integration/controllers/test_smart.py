from blacksheep.server.responses import json

from memonil.exponea.controllers import smart as uut


class TestSmart:

    @staticmethod
    def test_smart(client, supermocker):
        mocked = supermocker(
            uut.Smart,
            'hit_exponea'
        )
        mocked.return_value = {'time': 1337}
        resp = client.get('/api/smart?timeout=500')
        assert resp.json() == {'time': 1337}
        assert resp.status_code == 200

        mocked.return_value = json({
            'error': 'Requests have exceeded timeout period'
        }, status=504)
        resp = client.get('/api/smart?timeout=500')
        assert resp.json() == {
            'error': 'Requests have exceeded timeout period'
        }
        assert resp.status_code == 504
