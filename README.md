# Exponea engineering task

[![Codecov](https://codecov.io/gl/Memonil/exponea/branch/master/graph/badge.svg)](https://codecov.io/gl/Memonil/exponea)

## Usage
#### Environment varables

If you want to use monitoring instruments, supply the following environment variables:
- `SENTRY_DSN` (optional) - enables to send crash reports to [Sentry](https://sentry.io)
- `ENABLE_DATADOG` (optional) - enables to send various metrics and logs to [Datadog](https://www.datadoghq.com)
- `DD_API_KEY` (optional) - Datadog API key
- `DD_SITE` (optional) - destination site for Datadog metrics

#### Run
```
docker-compose -f docker-compose.yml up --build
```

#### Docs
This application provides OpenAPI docs running at [localhost/docs](http://localhost/docs)

#### Prometheus
Prometheus is running at [localhost:9090](http://localhost:9090/)

### Try it out
Exponea task server is also hosted at [https://exponea.memonil.com](https://exponea.memonil.com)

Docs are at [https://exponea.memonil.com](https://exponea.memonil.com/docs)

Prometheus is at [https://prometheus.memonil.com](https://prometheus.memonil.com)

## Tests
Tests are split into two categories.
- Integration tests which need to interact with the world
- Unit tests which can run without the need of outside world

```
pytest --cov
```
Tests will generate coverage report.

## Discussion
I broke the `prefer well-known open source libraries` desire by implementing relatively new and less known API framework
called [Blacksheep](https://www.neoteroi.dev/blacksheep).

This framework claims to be [one of the fastest](https://robertoprevato.github.io/Presenting-BlackSheep/) according to the author.
Beating even the most currently popular ones. Here is [updated benchmark](https://www.techempower.com/benchmarks/#section=data-r19&hw=ph&test=fortune&l=zijzen-1r).
 
Along with the speed it offers sophisticated dependency injection that makes a lot of stuff easier and cleaner.
This article has made me to give Blacksheep a try.

However it lacks some features, has less 3rd party integration libraries (obviously mostly because it is still pretty new). 
Thus has some of it's concepts like [MVC](https://www.neoteroi.dev/blacksheep/mvc-project-template) architecture 
designed improperly. This concept is actually really good but requires to follow **concrete** folder structure which I find very confusing.
Therefore I ended up bypassing this limitation by doing unused imports.

Monitoring is done with Datadog, Prometheus and Sentry. Datadog and Sentry require some environment variables to work.

Datadog monitors API statistics like number of endpoint hits, latency, error rates (200/504)
and is collecting logs from the entire app.

Prometheus collects endpoint hits, average endpoint latency, aggregates latency by endpoints
and tracks the number of requests currently being processed. 

Sentry tracks uncaught/unhandled errors.

#### Performance
Requests to Exponea testing server are done with [aiohttp client](https://docs.aiohttp.org/en/stable/client.html)
Aiohttp requests belong to the category of asyncio based concurrent requests that are tremendously fast.
Here is a [showcase](https://pawelmhm.github.io/asyncio/python/aiohttp/2016/04/22/asyncio-aiohttp.html)
of what requests on asyncio loop are capable of.

Blacksheep belongs to the category of ASGI frameworks so 
it runs on ASGI HTTP server [Uvicorn](https://www.uvicorn.org/). ASGI HTTP is able to serve clients concurrently.
Uvicorn runs behind [Gunicorn](https://gunicorn.org/) to
enable fully featured process management. One of its features is the ability to limit the maximum number of pending connections
to prevent server from being overloaded.
