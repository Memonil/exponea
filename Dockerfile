FROM python:3.9-alpine

WORKDIR /app

COPY *requirements.txt /app/

RUN apk add --no-cache --virtual=run-deps libstdc++ && \
    apk add --no-cache --virtual=.build-deps build-base linux-headers && \
    pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt -r test-requirements.txt && \
    apk del .build-deps

COPY . /app

# create directory for Gunicorn multiworker Prometheus mode
RUN mkdir -p /tmp/exponea/gunicorn-prometheus
ENV prometheus_multiproc_dir /tmp/exponea/gunicorn-prometheus

LABEL name=exponea version=dev \
      maintainer="Memonil <memonil@memonil.com>"
